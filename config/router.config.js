export default [
  // user
  {
    path: '/user',
    component: '../layouts/UserLayout',
    routes: [
      { path: '/user', redirect: '/user/login' },
      { path: '/user/login', component: './User/Login' },
      { path: '/user/register', component: './User/Register' },
      { path: '/user/register-result', component: './User/RegisterResult' },
    ],
  },
  // app
  {
    path: '/',
    component: '../layouts/BasicLayout',
    Routes: ['src/pages/Authorized'],
    authority: ['admin', 'user'],
    routes: [
      // dashboard
      { path: '/', redirect: '/dashboard/analysis' },
      {
        path: '/dashboard',
        name: '报表统计',
        icon: 'dashboard',
        routes: [
          {
            path: '/dashboard/analysis',
            name: '日统计',
            component: './Dashboard/Analysis',
          },
          {
            path: '/dashboard/workplace',
            name: '玩家统计',
            component: './Dashboard/Workplace',
          },
        ],
      },
      // forms
      {
        path: '/form',
        icon: 'form',
        name: '预警',
        routes: [
          {
            path: '/form/basic-form',
            name: '钻石获得',
            component: './Forms/BasicForm',
          },
          // {
          //   path: '/form/step-form',
          //   name: 'stepform',
          //   component: './Forms/StepForm',
          //   hideChildrenInMenu: true,
          //   routes: [
          //     {
          //       path: '/form/step-form',
          //       redirect: '/form/step-form/info',
          //     },
          //     {
          //       path: '/form/step-form/info',
          //       name: 'info',
          //       component: './Forms/StepForm/Step1',
          //     },
          //     {
          //       path: '/form/step-form/confirm',
          //       name: 'confirm',
          //       component: './Forms/StepForm/Step2',
          //     },
          //     {
          //       path: '/form/step-form/result',
          //       name: 'result',
          //       component: './Forms/StepForm/Step3',
          //     },
          //   ],
          // },
          // {
          //   path: '/form/advanced-form',
          //   name: 'advancedform',
          //   authority: ['admin'],
          //   component: './Forms/AdvancedForm',
          // },
        ],
      },
      // list
      {
        path: '/list',
        icon: 'table',
        name: '角色管理',
        routes: [
          {
            path: '/list/table-list',
            name: '角色基础信息',
            component: './List/TableList',
          },
          {
            path: '/list/card-list',
            name: '交易列表',
            component: './List/CardList',
          },

          // {
          //   path: '/list/basic-list',
          //   name: 'basiclist',
          //   component: './List/BasicList',
          // },
          // {
          //   path: '/list/card-list',
          //   name: 'cardlist',
          //   component: './List/CardList',
          // },
          // {
          //   path: '/list/search',
          //   name: 'searchlist',
          //   component: './List/List',
          //   routes: [
          //     {
          //       path: '/list/search',
          //       redirect: '/list/search/articles',
          //     },
          //     {
          //       path: '/list/search/articles',
          //       name: 'articles',
          //       component: './List/Articles',
          //     },
          //     {
          //       path: '/list/search/projects',
          //       name: 'projects',
          //       component: './List/Projects',
          //     },
          //     {
          //       path: '/list/search/applications',
          //       name: 'applications',
          //       component: './List/Applications',
          //     },
          //   ],
          // },
        ],
      },
      {
        path: '/account',
        icon: 'table',
        name: '账号管理',
        authority: ['admin'],
        routes: [
          {
            path: '/account/user-list',
            name: '账号信息',
            component: '/List/BasicList',

          },
        ],
      },
      {
        component: '404',
      },
    ],
  },
];
