import { queryFakeList, updateTicket, ticketFetch, updateFakeList,pAccountList,NewPAccount,submitmaxValue,updatePwd } from '@/services/api';

export default {
  namespace: 'list',

  state: {
    list: [],
    pAccountList: [],
    TicketStatus:null,
  },

  effects: {
    *fetch({ payload }, { call, put }) {
      const response = yield call(queryFakeList, payload);
      console.log("response is",response)
      yield put({
        type: 'queryList',
        payload: response.code === 200 ? response.info : [],
      });
    },
    *ticketFetch({ payload }, { call, put }) {
      const response = yield call(ticketFetch, payload);
      yield put({
        type: 'TicketStatus',
        payload: response.code === 200 ? response.info : 0,
      });
    },
    *submit({ payload }, { call, put }) {
      const response = yield call(updateFakeList, payload); // post
      yield put({
        type: 'queryList',
        payload: response.code === 200 ? response.info : [],
      });
    },

    *submitTicket({ payload }, { call, put }) {
      const response = yield call(updateTicket, payload); // post
      console.log("submitTicket response",response)
      yield put({
        type: 'TicketStatus',
        payload: response.code === 200 ? response.info : 0,
      });
    },

    *submitmaxValue({ payload }, { call, put }) {
      const response = yield call(submitmaxValue, payload); // post
      console.log("submitTicket response",response)
      yield put({
        type: 'TicketStatus',
        payload: response.code === 200 ? response.info : 0,
      });
    },


    *updatePwd({ payload }, { call, put }) {
      const response = yield call(updatePwd, payload); // post
      console.log("updatePwd response",response)
      // yield put({
      //   type: 'updatePwd',
      //   payload: response.code === 200 ? response.info : 0,
      // });
    },

    *pAccountList({ payload }, { call, put }) {
      const response = yield call(pAccountList, payload); // post
      console.log("pAccountList response",response)
      yield put({
        type: 'PAccountList',
        payload: response.code === 200 ? response.info : 0,
      });
    },

    *submitNewPAccount({ payload }, { call, put }) {
      console.log("submitNewPAccount response",payload)
      const response = yield call(NewPAccount, payload); // post
      console.log("submitNewPAccount response",response)
      yield put({
        type: 'PAccountList',
        payload: response.code === 200 ? response.info : 0,
      });
    },
  },

  reducers: {
    queryList(state, action) {
      return {
        ...state,
        list: action.payload,
      };
    },
    TicketStatus(state, action) {
      return {
        ...state,
        TicketStatus: action.payload,
      };
    },
    appendList(state, action) {
      return {
        ...state,
        list: state.list.concat(action.payload),
      };
    },
    PAccountList(state, action) {
      return {
        ...state,
        pAccountList: action.payload,
      };
    },
  },
};
