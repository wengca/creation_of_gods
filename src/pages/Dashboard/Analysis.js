import React, { Component } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Form,
  Input,
  Select,
  Button,
  InputNumber,
  Modal,
  message,
  Badge,
  Divider,
  Steps,
  Icon,
  Card,
  Tabs,
  Table,
  Radio,
  DatePicker,
  Tooltip,
  Menu,
  Dropdown,
} from 'antd';
import { formatMessage, FormattedMessage } from 'umi/locale';
import styles2 from '../List/TableList.less';

const FormItem = Form.Item;
import {
  ChartCard,
  MiniArea,
  MiniBar,
  MiniProgress,
  Field,
  Bar,
  Pie,
  TimelineChart,
} from '@/components/Charts';
import Trend from '@/components/Trend';
import NumberInfo from '@/components/NumberInfo';
import numeral from 'numeral';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import Yuan from '@/utils/Yuan';
import { getTimeDistance } from '@/utils/utils';

import styles from './Analysis.less';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import moment from "moment/moment";

const { TabPane } = Tabs;
const { RangePicker } = DatePicker;

@connect(({ chart, loading }) => ({
  chart,
  loading: loading.effects['chart/fetch'],
}))
class Analysis extends Component {
  constructor(props) {
    super(props);
  }

  state = {
    salesType: 'all',
    currentTabKey: '',
    rangePickerValue:"",
    loading: true,
  };

  componentDidMount() {
    const { dispatch } = this.props;
    this.reqRef = requestAnimationFrame(() => {
      dispatch({
        type: 'chart/fetch',
        payload:{
          startTime:new Date().getTime() - 7 *24 * 3600 * 1000,
          endTime:new Date().getTime()
        }
      });
      this.timeoutId = setTimeout(() => {
        this.setState({
          loading: false,
        });
      }, 600);
    });
  }



  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'chart/clear',
    });
    cancelAnimationFrame(this.reqRef);
    clearTimeout(this.timeoutId);
  }

  handleSearch = e => {
    e.preventDefault();
    const { dispatch } = this.props;
    const { rangePickerValue } = this.state

    dispatch({
      type: 'chart/fetch',
      payload:{
        startTime: new Date(rangePickerValue[0]).getTime(),
        endTime:new Date(rangePickerValue[1]).getTime(),
      }
    });
  };

  changeTine = (date,dateString) => {
    console.log("date",date)
    console.log("dateString",dateString)
    this.setState({
      rangePickerValue: dateString,
    });

  };

  handleChangeSalesType = e => {
    this.setState({
      salesType: e.target.value,
    });
  };

  handleTabChange = key => {
    this.setState({
      currentTabKey: key,
    });
  };



  isActive(type) {
    const { rangePickerValue } = this.state;
    const value = getTimeDistance(type);
    if (!rangePickerValue[0] || !rangePickerValue[1]) {
      return '';
    }
    if (
      rangePickerValue[0].isSame(value[0], 'day') &&
      rangePickerValue[1].isSame(value[1], 'day')
    ) {
      return styles.currentDate;
    }
    return '';
  }

  renderForm() {
    return this.renderSimpleForm();
  }

  renderSimpleForm() {
    // const {
    //   form: { getFieldDecorator },
    // } = this.props;
    //
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={8}>
            <RangePicker
              style={{ width: '100%' }}
              placeholder={[
                formatMessage({ id: 'form.date.placeholder.start' }),
                formatMessage({ id: 'form.date.placeholder.end' }),
              ]}
              onChange={this.changeTine}
            />
          </Col>

          <Col md={8} sm={8}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  render() {
    const { rangePickerValue, salesType, loading: stateLoading, currentTabKey } = this.state;
    const { chart, loading: propsLoading } = this.props;

    const {
      offlineData,
      offlineChartData,
      yestInfo,
      tableDate,
    } = chart;
    let tableDateInfo = offlineChartData[0]
    console.log('offlineChartData', offlineChartData);
    console.log('offlineData', offlineData);
    console.log('tableDate', tableDate);
    console.log('yestInfo', yestInfo);

    const loading = propsLoading || stateLoading;

    const columns2 = [
      {
        title: '时间',
        dataIndex: 'x',
        render: val => <span>{moment(val).format('YYYY-MM-DD')}</span>,
      },
      // {
      //   title: '总人数',
      //   dataIndex: 'totalPeople',
      //   key: 'totalPeople',
      // },
      // {
      //   title: '新增人数',
      //   dataIndex: 'newPeople',
      //   key: 'newPeople',
      //   className: styles.alignRight,
      // },
      // {
      //   title: '活跃',
      //   dataIndex: 'activeNum',
      //   key: 'activeNum',
      // },
      {
        title: '交易',
        dataIndex: 'y1',
      },
    ];

    const activeKey = currentTabKey || (offlineData[0] && offlineData[0].name);

    const CustomTab = ({ data, currentTabKey: currentKey }) => (
      <Row gutter={8} style={{ width: 168, margin: '8px 0' }}>
        <Col span={12}>
          <NumberInfo gap={2} total={data.name} theme={currentKey !== data.name && 'light'} />
        </Col>
      </Row>
    );

    const topColResponsiveProps = {
      xs: 24,
      sm: 12,
      md: 12,
      lg: 12,
      xl: 8,
      style: { marginBottom: 24 },
    };

    return (
      <PageHeaderWrapper
      >

      <GridContent>
        <Row gutter={24}>
          <Col {...topColResponsiveProps}>
            <ChartCard
              bordered={false}
              title={'昨日总人数'}
              action={
                <Tooltip
                  title={
                    <FormattedMessage id="app.analysis.introduce" defaultMessage="introduce" />
                  }
                />
              }
              loading={loading}
              total={yestInfo[0]}
              contentHeight={24}
            />
          </Col>

          <Col {...topColResponsiveProps}>
            <ChartCard
              bordered={false}
              title={'昨日新增人数'}
              action={
                <Tooltip
                  title={
                    <FormattedMessage id="app.analysis.introduce" defaultMessage="introduce" />
                  }
                />
              }
              loading={loading}
              total={yestInfo[1]}
              contentHeight={24}
            />
          </Col>

          <Col {...topColResponsiveProps}>
            <ChartCard
              bordered={false}
              title={'昨日活跃'}
              action={
                <Tooltip
                  title={
                    <FormattedMessage id="app.analysis.introduce" defaultMessage="introduce" />
                  }
                >
                  <Icon type="info-circle-o" />
                </Tooltip>
              }
              loading={loading}
              total={yestInfo[2]}
              contentHeight={24}
            />
          </Col>

          <Col {...topColResponsiveProps}>
            <ChartCard
              bordered={false}
              title={'昨日玩家总产量之和'}
              action={
                <Tooltip
                  title={
                    <FormattedMessage id="app.analysis.introduce" defaultMessage="introduce" />
                  }
                >
                  <Icon type="info-circle-o" />
                </Tooltip>
              }
              loading={loading}
              total={yestInfo[3]}
              contentHeight={24}
            />
          </Col>


          <Col {...topColResponsiveProps}>
            <ChartCard
              bordered={false}
              title={'昨日新增杨任玩家数量'}
              action={
                <Tooltip
                  title={
                    <FormattedMessage id="app.analysis.introduce" defaultMessage="introduce" />
                  }
                >
                  <Icon type="info-circle-o" />
                </Tooltip>
              }
              loading={loading}
              total={yestInfo[4]}
              contentHeight={24}
            />
          </Col>

          <Col {...topColResponsiveProps}>
            <ChartCard
              bordered={false}
              title={'昨日升级杨任以上玩家数量'}
              action={
                <Tooltip
                  title={
                    <FormattedMessage id="app.analysis.introduce" defaultMessage="introduce" />
                  }
                >
                  <Icon type="info-circle-o" />
                </Tooltip>
              }
              loading={loading}
              total={yestInfo[5]}
              contentHeight={24}
            />
          </Col>
        </Row>

        <Card bordered={false}>
          <div className={styles2.tableList}>
            <div className={styles2.tableListForm}>{this.renderForm()}</div>
          </div>
        </Card>

        <Card
          loading={loading}
          className={styles.offlineCard}
          bordered={false}
          bodyStyle={{ padding: '0 0 32px 0' }}
          style={{ marginTop: 32 }}
        >
          <Tabs activeKey={activeKey} onChange={this.handleTabChange}>
            {offlineData.map((shop, tabIndex) => (
              <TabPane tab={<CustomTab data={shop} currentTabKey={activeKey} />} key={shop.name}>
                <div style={{ padding: '0 24px' }}>
                  <TimelineChart
                    height={400}
                    data={offlineChartData[tabIndex]}
                    titleMap={{
                      y1: shop.name,
                      // y2: formatMessage({ id: 'app.analysis.payments' }),
                    }}
                  />
                </div>
              </TabPane>
            ))}
          </Tabs>
        </Card>
          <Card
            loading={loading}
            className={styles.offlineCard}
            bordered={false}
            bodyStyle={{ padding: '0 0 32px 0' }}
            style={{ marginTop: 32 }}
          >
          <Table
            rowKey={record => record.index}
            size="small"
            columns={columns2}
            dataSource={tableDateInfo}
            pagination={{
              style: { marginBottom: 0 },
              pageSize: 7,
            }}
          />
        </Card>
      </GridContent>

      </PageHeaderWrapper>
    );
  }
}

export default Analysis;
