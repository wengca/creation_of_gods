import React, { Component } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Form,
  Input,
  Select,
  Button,
  InputNumber,
  Modal,
  message,
  Badge,
  Divider,
  Steps,
  Icon,
  Card,
  Tabs,
  Table,
  Radio,
  DatePicker,
  Tooltip,
  Menu,
  Dropdown,
} from 'antd';
import { formatMessage, FormattedMessage } from 'umi/locale';
import styles2 from '../List/TableList.less';

const FormItem = Form.Item;
import {
  ChartCard,
  MiniArea,
  MiniBar,
  MiniProgress,
  Field,
  Bar,
  Pie,
  TimelineChart,
} from '@/components/Charts';
import Trend from '@/components/Trend';
import NumberInfo from '@/components/NumberInfo';
import numeral from 'numeral';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import Yuan from '@/utils/Yuan';
import { getTimeDistance } from '@/utils/utils';

import styles from './Analysis.less';

const { TabPane } = Tabs;
const { RangePicker } = DatePicker;

@connect(({ chart, loading }) => ({
  chart,
  loading: loading.effects['chart/fetch'],
}))

@Form.create()

class Workplace extends Component {
  constructor(props) {
    super(props);
  }

  state = {
    salesType: 'all',
    selectTime:"",
    currentTabKey: '',
    rangePickerValue: getTimeDistance('year'),
    loading: true,
  };

  componentDidMount() {
    const { dispatch } = this.props;
    this.reqRef = requestAnimationFrame(() => {
      dispatch({
        type: 'chart/fetchHeroInfo',
        payload:{
          time:new Date().getTime(),
        }
      });
      this.timeoutId = setTimeout(() => {
        this.setState({
          loading: false,
        });
      }, 600);
    });
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'chart/clear',
    });
    cancelAnimationFrame(this.reqRef);
    clearTimeout(this.timeoutId);
  }


  changeTime = (date, dateString) =>{
    console.log("changeTime e is:",date)
    console.log("changeTime e is:",dateString)
    this.setState({
      selectTime :dateString
    })
  }

  handleSearch = e => {
    e.preventDefault();
    const { selectTime } = this.state;
    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      console.log("fieldsValue is :",fieldsValue)
      const values = {
        ...fieldsValue,
      };

      console.log("values is :",values)
      console.log("selectTime is :",selectTime)

      dispatch({
        type: 'chart/fetchHeroInfo',
        payload:{
          time:new Date(selectTime).getTime(),
        }
      });

    });
  };



  renderForm() {
    return this.renderSimpleForm();
  }

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={8}>

            <FormItem label="用户id">
              {getFieldDecorator('time')( <DatePicker
                style={{ width: '100%' }}
                placeholder={"选择时间"}
                onChange={this.changeTime}

              />)}
            </FormItem>
          </Col>

          <Col md={8} sm={8}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit" >
                查询
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  render() {
    const { rangePickerValue, salesType, loading: stateLoading, currentTabKey } = this.state;
    const { chart, loading: propsLoading } = this.props;

    const {
      offlineData,
      offlineChartData,
      salesTypeData,
      salesTypeDataOnline,
      salesTypeDataOffline,
      heroLists,
    } = chart;

    console.log('offlineChartData', offlineChartData);
    console.log('offlineData', offlineData);
    console.log('HeroList', heroLists);

    const loading = propsLoading || stateLoading;

    const columns2 = [
      {
        title: '英雄名称',
        dataIndex: 'x',
      },
      {
        title: '拥有人数',
        dataIndex: 'y',
        key: 'y',
      },

    ];

    return (
      <GridContent>

        <Card bordered={false}>
          <div className={styles2.tableList}>
            <div className={styles2.tableListForm}>{this.renderForm()}</div>
          </div>
        </Card>

        <Card
          loading={loading}
          className={styles.offlineCard}
          bordered={false}
          bodyStyle={{ padding: '0 0 32px 0' }}
          style={{ marginTop: 32 }}
        >

          <Bar
            height={400}
            data={heroLists}
            // title={"人数"}
            autoLabel={false}
          />

          <Table
            rowKey={record => record.index}
            size="large"
            columns={columns2}
            dataSource={heroLists}
            pagination={{
              style: { marginBottom: 0 },
              pageSize: 10,
            }}
          />
        </Card>
      </GridContent>
    );
  }
}

export default Workplace;
