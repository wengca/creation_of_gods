import { fakeChartData,getHeroNum } from '@/services/api';
import {message} from "antd/lib/index";

export default {
  namespace: 'chart',

  state: {
    offlineData: [],
    offlineChartData: [],
    tableDate: [],
    heroLists: [],
    yestInfo: [],
    loading: false,
  },

  effects: {

    *fetch({ payload }, { call, put }) {
      const response = yield call(fakeChartData,payload);
      if(response.offlineChartData[0].length <=0 ){
        message.error('没有数据');
        return
      }

      yield put({
        type: 'save',
        payload: response,
      });
    },

    *fetchSalesData(_, { call, put }) {
      const response = yield call(fakeChartData);
      console.log("response is:",response)
      yield put({
        type: 'save',
        payload: {
          salesData: response.salesData,
        },
      });
    },

    *fetchHeroInfo({payload}, { call, put }) {
      console.log("fetchHeroInfo payload is:",payload)
      const response = yield call(getHeroNum,payload);
      yield put({
        type: 'save',
        payload: {
          heroLists: response.info,
        },
      });
    },
  },

  reducers: {
    save(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
    clear() {
      return {
        visitData: [],
        visitData2: [],
        salesData: [],
        searchData: [],
        offlineData: [],
        offlineChartData: [],
        salesTypeData: [],
        salesTypeDataOnline: [],
        salesTypeDataOffline: [],
        radarData: [],
        yestInfo: [],
      };
    },
  },
};
