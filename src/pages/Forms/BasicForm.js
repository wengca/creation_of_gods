import React, { Component } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Form,
  Input,
  Select,
  Button,
  InputNumber,
  Modal,
  message,
  Badge,
  Divider,
  Steps,
  Icon,
  Card,
  Tabs,
  Table,
  Radio,
  DatePicker,
  Tooltip,
  Menu,
  Dropdown,
} from 'antd';
import { formatMessage, FormattedMessage } from 'umi/locale';
import styles2 from '../List/TableList.less';

const FormItem = Form.Item;
import {
  ChartCard,
  MiniArea,
  MiniBar,
  MiniProgress,
  Field,
  Bar,
  Pie,
  TimelineChart,
} from '@/components/Charts';
import Trend from '@/components/Trend';
import NumberInfo from '@/components/NumberInfo';
import numeral from 'numeral';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import Yuan from '@/utils/Yuan';
import { getTimeDistance } from '@/utils/utils';
import moment from 'moment/moment';

import styles from '../Dashboard/Analysis.less';

const { TabPane } = Tabs;
const { RangePicker } = DatePicker;

@connect(({ form, loading }) => ({
  form,
  loading: loading.effects['form/fetch'],
}))
class BaseFrorms extends Component {
  constructor(props) {
    super(props);
  }

  state = {
    salesType: 'all',
    currentTabKey: '',
    rangePickerValue: moment(new Date()).subtract(0, 'days'),
    loading: true,
    traceWarn:[],
  };

  componentDidMount() {
    const { dispatch } = this.props;
    this.reqRef = requestAnimationFrame(() => {
      dispatch({
        type: 'form/fetchTraceWarn',
        payload:{
          time:moment(new Date()).format("YYYY-MM-DD"),
        }
      });
      this.timeoutId = setTimeout(() => {
        this.setState({
          loading: false,
        });
      }, 600);
    });
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'chart/clear',
    });
    cancelAnimationFrame(this.reqRef);
    clearTimeout(this.timeoutId);
  }


  handleSearch = e => {
    e.preventDefault();
    const { dispatch } = this.props;
    const { rangePickerValue } = this.state
    console.log("3333333333333333",rangePickerValue.format("YYYY-MM-DD"))

    dispatch({
      type: 'form/fetchTraceWarn',
      payload:{
        time:rangePickerValue.format("YYYY-MM-DD"),
      }
    });
  };

  selectDate = type => {
    // const { dispatch } = this.props;
    console.log("xxxxxxxxxxxxxxxx",type.format("YYYY-MM-DD"))
    this.setState({
      rangePickerValue: type,
    });

    // dispatch({
    //   type: 'chart/fetchSalesData',
    // });
  };

  renderForm() {
    return this.renderSimpleForm();
  }

  renderSimpleForm() {
    // const {
    //   form: { getFieldDecorator },
    // } = this.props;
    //
    const { rangePickerValue } = this.state;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={8}>
            <DatePicker
              style={{ width: '100%' }}
              placeholder={ "选择时间"}
              onChange={this.selectDate}
              value={rangePickerValue}
            />
          </Col>

          <Col md={8} sm={8}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  render() {
    const {  loading: stateLoading, } = this.state;
    const { form, loading: propsLoading } = this.props;
   const { traceWarn } = form
    console.log('traceWarn', traceWarn);
    console.log('this.props', this.props);
    const loading = propsLoading || stateLoading;
    const columns2 = [
      {
        title: '用户名',
        dataIndex: 'uuid',
      },
      {
        title: '用户昵称',
        dataIndex: 'nick',
      },
      {
        title: '等级',
        dataIndex: 'lv',
      },
      {
        title: '获得的钻石',
        dataIndex: 'itemValue',
      },
      {
        title: '时间',
        dataIndex: 'time',
      },

    ];

    return (
      <GridContent>

        <Card bordered={false}>
          <div className={styles2.tableList}>
            <div className={styles2.tableListForm}>{this.renderForm()}</div>
          </div>
        </Card>

        <Card
          loading={loading}
          className={styles.offlineCard}
          bordered={false}
          bodyStyle={{ padding: '0 0 32px 0' }}
          style={{ marginTop: 32 }}
        >

          <Table
            rowKey={record => record.index}
            size="large"
            columns={columns2}
            dataSource={traceWarn}
            pagination={{
              style: { marginBottom: 0 },
              pageSize: 10,
            }}
          />
        </Card>
      </GridContent>
    );
  }
}

export default BaseFrorms;
