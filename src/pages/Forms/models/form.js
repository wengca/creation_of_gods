import { routerRedux } from 'dva/router';
import { message } from 'antd';
import { fakeSubmitForm ,fetchTraceWarn} from '@/services/api';

export default {
  namespace: 'form',

  state: {
    step: {
      payAccount: 'ant-design@alipay.com',
      receiverAccount: 'test@example.com',
      receiverName: 'Alex',
      amount: '500',
    },
    traceWarn:[],
  },

  effects: {
    *submitRegularForm({ payload }, { call }) {
      yield call(fakeSubmitForm, payload);
      message.success('提交成功');
    },
    *submitStepForm({ payload }, { call, put }) {
      yield call(fakeSubmitForm, payload);
      yield put({
        type: 'saveStepFormData',
        payload,
      });
      yield put(routerRedux.push('/form/step-form/result'));
    },
    *submitAdvancedForm({ payload }, { call }) {
      yield call(fakeSubmitForm, payload);
      message.success('提交成功');
    },

    *fetchTraceWarn({payload}, { call, put }) {
      const response =  yield call(fetchTraceWarn, payload);
      console.log("response",response)
      yield put({
        type: 'save',
        payload: {
          traceWarn: response.info,
        },
      });
    },
  },


  reducers: {
    saveStepFormData(state, { payload }) {
      return {
        ...state,
        step: {
          ...state.step,
          ...payload,
        },
      };
    },

    save(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },

  },
};
