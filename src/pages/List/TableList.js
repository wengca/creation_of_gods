import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Steps,
  Radio,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';

import styles from './TableList.less';

const FormItem = Form.Item;
const { TextArea } = Input;
const { Option } = Select;
const RadioGroup = Radio.Group;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');
const statusMap = ["default",'default', 'processing'];
const status = ["",'正常', '封号'];

const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };
  return (
    <Modal
      destroyOnClose
      title="新建规则"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="描述">
        {form.getFieldDecorator('desc', {
          rules: [{ required: true, message: '请输入至少五个字符的规则描述！', min: 5 }],
        })(<Input placeholder="请输入" />)}
      </FormItem>
    </Modal>
  );
});

@Form.create()
class UpdateForm extends PureComponent {
  constructor(props) {
    super(props);
  console.log("props.values",props)
    this.state = {
      formVals: {
        id: props.values.id,
        nick: props.values.nick,
        ban: props.values.ban,
      },
      currentStep: 0,
    };

    this.formLayout = {
      labelCol: { span: 7 },
      wrapperCol: { span: 13 },
    };
  }

  handleNext = currentStep => {
    const { form, handleUpdate } = this.props;
    const { formVals: oldValue } = this.state;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const formVals = { ...oldValue, ...fieldsValue };
      this.setState(
        {
          formVals,
        },
        () => {
          handleUpdate(formVals);
        }
      );
    });
  };

  backward = () => {
    const { currentStep } = this.state;
    this.setState({
      currentStep: currentStep - 1,
    });
  };

  forward = () => {
    const { currentStep } = this.state;
    this.setState({
      currentStep: currentStep + 1,
    });
  };

  renderContent = (currentStep, formVals) => {
    const { form } = this.props;
    let initValue = formVals.ban;
    console.log("renderContent formVals is",formVals )
    if(initValue === 2){
      initValue = "解封"
    }else {
      initValue = "封号"
    }
    console.log("initValue", typeof initValue, initValue)
    return [
      <FormItem key="target" {...this.formLayout} label="修改状态">
        {form.getFieldDecorator('target', {
          initialValue: initValue,
        })(
          <Select style={{ width: '100%' }}>
            <Option style={{display:formVals.ban === 2?"none":""}} value="2">封号</Option>
            <Option style={{display:formVals.ban === 1?"none":""}} value="1">解封</Option>
          </Select>
        )}
      </FormItem>,

    ];
  };

  renderFooter = currentStep => {
    const { handle } = this.props;
    return [
      <Button key="cancel" onClick={() => handleUpdateModalVisible()}>
        取消
      </Button>,
      <Button key="submit" type="primary" onClick={() => this.handleNext(1)}>
        完成
      </Button>,
    ];
  };

  render() {
    const { updateModalVisible, handleUpdateModalVisible } = this.props;
    const { currentStep, formVals } = this.state;
    console.log("formVals",formVals)
    return (

      <Modal
        width={640}
        bodyStyle={{ padding: '32px 40px 48px' }}
        destroyOnClose
        title="封号"
        visible={updateModalVisible}
        footer={this.renderFooter(currentStep)}
        onCancel={() => handleUpdateModalVisible()}
      >

        {this.renderContent(currentStep, formVals)}
      </Modal>
    );
  }
}

@Form.create()
class SendItems extends PureComponent {
  constructor(props) {
    super(props);
    console.log("props.values",props)
    this.state = {
      formVals: {
        id: props.values.id,
        nick: props.values.nick,
        ban: props.values.ban,
      },
      currentStep: 0,
    };

    this.formLayout = {
      labelCol: { span: 7 },
      wrapperCol: { span: 13 },
    };
  }

  handleNext = type => {
    const { form, handleUpdate } = this.props;
    const { formVals: oldValue } = this.state;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const formVals = { ...oldValue, ...fieldsValue ,type};
      this.setState(
        {
          formVals,
        },
        () => {
          handleUpdate(formVals);
        }
      );
    });
  };

  backward = () => {
    const { currentStep } = this.state;
    this.setState({
      currentStep: currentStep - 1,
    });
  };

  forward = () => {
    const { currentStep } = this.state;
    this.setState({
      currentStep: currentStep + 1,
    });
  };

  renderContent = (currentStep, formVals) => {
    const { form } = this.props;
    let initValue = formVals.ban;
    console.log("renderContent formVals is",formVals )

    console.log("initValue", typeof initValue, initValue)
    return [
      <FormItem key="ItemId" {...this.formLayout} label="物品">
        {form.getFieldDecorator('ItemId', {
          rules: [{ required: true }],
          initialValue: null,
        })(
          <Select style={{ width: '100%' }}>
            <Option  value="0">红水晶</Option>
            <Option  value="1">蓝水晶</Option>
            <Option  value="2">收徒券</Option>
          </Select>
        )}
      </FormItem>,

      <FormItem key="Num" {...this.formLayout} label="数量">
        {form.getFieldDecorator('Num', {
          initialValue: 0,
        })(
          <Input>

          </Input>
        )}
      </FormItem>,

    ];
  };

  renderFooter = currentStep => {
    const { handleSentItemVisible } = this.props;
    return [
      <Button key="cancel" onClick={() => handleSentItemVisible()}>
        取消
      </Button>,
      <Button key="submit" type="primary" onClick={() => this.handleNext(2)}>
        完成
      </Button>,
    ];
  };

  render() {
    const { sendItemVisible, handleSentItemVisible } = this.props;
    const { currentStep, formVals } = this.state;
    console.log("formVals",formVals)
    return (

      <Modal
        width={640}
        bodyStyle={{ padding: '32px 40px 48px' }}
        destroyOnClose
        title="封号"
        visible={sendItemVisible}
        footer={this.renderFooter(currentStep)}
        onCancel={() => handleSentItemVisible()}
      >

        {this.renderContent(currentStep, formVals)}
      </Modal>
    );
  }
}


/* eslint react/no-multi-comp:0 */
@connect(({ rule, loading }) => ({
  rule,
  loading: loading.models.rule,
}))
@Form.create()
class TableList extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    sendItemVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    stepFormValues: {},
  };

  columns = [
    {
      title: '用户id',
      dataIndex: 'uuid',
    },
    {
      title: '用户名称',
      dataIndex: 'nick',
    },

    {
      title: '状态',
      dataIndex: 'ban',
      render(val) {
        return <Badge status={statusMap[val]} text={status[val]} />;
      },
    },
    {
      title: '创建时间',
      dataIndex: 'create_time',
      sorter: true,
      render: val => <span>{moment(val).format('YYYY-MM-DD HH:mm:ss')}</span>,
    },
    {
      title: '等级',
      dataIndex: 'lv',

    },
    {
      title: '功勋数量',
      dataIndex: 'feats',

    },
    {
      title: '师傅id',
      dataIndex: 'masterId',

    },
    {
      title: '师傅名称',
      dataIndex: 'masterNick',

    },
    {
      title: '电话',
      dataIndex: 'phone',

    },
    {
      title: '一代徒弟数量',
      dataIndex: 'num',

    },
    {
      title: '一代徒弟战力总和',
      dataIndex: 'power',

    },
    {
      title: '闲置蓝水晶数量',
      dataIndex: 'blue_crystal',

    },
    {
      title: '闲置红水晶数量',
      dataIndex: 'red_crystal',

    },
    {
      title: '操作',
      render: (text, record) => (
        <Row>
          <Col>
            <Fragment>
              <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改状态</a>
            </Fragment>,
          </Col>
          <Col>
            <Fragment>
              <a onClick={() => this.handleSentItemVisible(true, record)}>赠送道具</a>
            </Fragment>,
          </Col>
        </Row>
      ),
    },
  ];

  componentDidMount() {
    const { dispatch } = this.props;
    // dispatch({
    //   type: 'rule/fetch',
    // });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'rule/fetch',
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    console.log("form is:",form)
    console.log("dispatch is:",dispatch)
    this.setState({
      formValues: {},
    });
    dispatch({
      type: 'rule/fetch',
      payload: {},
    });
  };

  // toggleForm = () => {
  //   const { expandForm } = this.state;
  //   this.setState({
  //     expandForm: !expandForm,
  //   });
  // };

  handleMenuClick = e => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;

    if (!selectedRows) return;
    switch (e.key) {
      case 'remove':
        dispatch({
          type: 'rule/remove',
          payload: {
            key: selectedRows.map(row => row.key),
          },
          callback: () => {
            this.setState({
              selectedRows: [],
            });
          },
        });
        break;
      default:
        break;
    }
  };

  handleSelectRows = rows => {
    console.log("ddddddddddddddddddd")
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;
    console.log("dispatch",dispatch)


    form.validateFields((err, fieldsValue) => {
      console.log("fieldsValue",fieldsValue)

      if (err) return;

      const values = {
        ...fieldsValue,
        // updatedAt: fieldsValue.updatedAt && fieldsValue.updatedAt.valueOf(),
      };
      console.log("values",values)

      this.setState({
        formValues: values,
      });

      dispatch({
        type: 'rule/fetch',
        payload: values,
      });
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    console.log("record is:",record)
    this.setState({
      updateModalVisible: !!flag,
      stepFormValues: record || {},
    });
  };

  handleSentItemVisible = (flag, record) => {
    console.log("record is:",record)
    this.setState({
      sendItemVisible: !!flag,
      stepFormValues: record || {},

    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'rule/add',
      payload: {
        desc: fields.desc,
      },
    });

    message.success('添加成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { sendItemVisible } = this.state;
    console.log("update info is:",fields)
    const type = fields.type
    switch (sendItemVisible) {
      case false:
        console.log("fields is:",fields)
        if(fields.target === "封号"){
          fields.ban = 2
        }else {
          fields.ban = 1
        }
        console.log("fields is:",fields)
        dispatch({
          type: 'rule/update',
          payload: {
            nick: fields.nick,
            id: fields.id,
            ban: fields.ban,
          },
        });
        break
      case true:
        dispatch({
          type: 'rule/sendItem',
          payload: {
            ...fields
          },
        });
    }

    let that = this;
    setTimeout( function () {
      dispatch({
        type: 'rule/fetch',
        payload: {
          ...fields
        }
      });

      message.success('配置成功');
      that.handleUpdateModalVisible();
    },500)

  };

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="用户id">
              {getFieldDecorator('uuid')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="用户名称">
              {getFieldDecorator('nick')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>

          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }


  renderForm() {
    const { expandForm } = this.state;
    return expandForm ? this.renderAdvancedForm() : this.renderSimpleForm();
  }

  render() {
    const {
      rule: { data },
      loading,
    } = this.props;
    // console.log("data",data,data.length)
    // if(data.length <=0) return
    const newData = {};
    newData.list = data;
    newData.pagination={total: 1, pageSize: 10, current: 1}
    console.log("data",newData)
    const { selectedRows, modalVisible, updateModalVisible, sendItemVisible,stepFormValues } = this.state;
    const menu = (
      <Menu onClick={this.handleMenuClick} selectedKeys={[]}>
        <Menu.Item key="remove">删除</Menu.Item>
        <Menu.Item key="approval">批量审批</Menu.Item>
      </Menu>
    );

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };


    const SentItemVisible = {
      handleSentItemVisible: this.handleSentItemVisible,
      handleUpdate: this.handleUpdate,
    };

    return (
      <PageHeaderWrapper title="查询表格">
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderForm()}</div>
            <div className={styles.tableListOperator}>
            </div>
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              data={newData}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
            />
          </div>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} />
        {stepFormValues && Object.keys(stepFormValues).length ? (
          <UpdateForm
            {...updateMethods}
            updateModalVisible={updateModalVisible}
            values={stepFormValues}
          />
        ) : null}


        <CreateForm {...parentMethods} modalVisible={modalVisible} />
        {stepFormValues && Object.keys(stepFormValues).length ? (
          <SendItems
            {...SentItemVisible}
            sendItemVisible={sendItemVisible}
            values={stepFormValues}
          />
        ) : null}
      </PageHeaderWrapper>
    );
  }
}

export default TableList;
