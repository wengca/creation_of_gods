import { queryRule, removeRule, addRule, updateRule,sendItem } from '@/services/api';
import {message} from "antd/lib/index";

export default {
  namespace: 'rule',

  state: {
    data: [],
  },

  effects: {
    *fetch({ payload }, { call, put }) {
      const response = yield call(queryRule, payload);
      console.log("xxxxxxxxxxxxxxxxxxxxxx")
      if(response.code !== 200) {
        message.error('Failed to load msg');
        yield put({
          type: 'save',
          payload: [],
        });
        return
      }

      yield put({
        type: 'save',
        payload: response.info,
      });
    },
    *add({ payload, callback }, { call, put }) {
      const response = yield call(addRule, payload);
      yield put({
        type: 'save',
        payload: response,
      });
      if (callback) callback();
    },
    *remove({ payload, callback }, { call, put }) {
      const response = yield call(removeRule, payload);
      yield put({
        type: 'save',
        payload: response,
      });
      if (callback) callback();
    },

    *update({ payload, callback }, { call, put }) {
      const response = yield call(updateRule, payload);
      yield put({
        type: 'save',
        payload: response.info,
      });
      if (callback) callback();
    },

    *sendItem({ payload, callback }, { call, put }) {
      const response = yield call(sendItem, payload);
      // yield put({
      //   type: 'save',
      //   payload: response.info,
      // });
      if (callback) callback();
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        data: action.payload,
      };
    },
  },
};
