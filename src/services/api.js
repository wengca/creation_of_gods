import { stringify } from 'qs';
import request from '@/utils/request';

export async function queryProjectNotice() {
  return request('/api/project/notice');
}

export async function queryActivities() {
  return request('/api/activities');
}

export async function queryRule(params) {
  return request(`/api/rule?${stringify(params)}`);
}


export async function updateRule(params) {
  return request('/api/updateAccountBan', {
    method: 'POST',
    body: {
      ...params,
      method: 'update',
    },
  });
}

export async function fakeSubmitForm(params) {
  return request('/api/forms', {
    method: 'POST',
    body: params,
  });
}

export async function fakeChartData(params) {
  console.log("fakeChartData params :",params)
  return request(`/api/fake_chart_data?${stringify(params)}`);
}

export async function queryTags() {
  return request('/api/tags');
}

export async function queryBasicProfile() {
  return request('/api/profile/basic');
}

export async function queryAdvancedProfile() {
  return request('/api/profile/advanced');
}

export async function queryFakeList(params) {
  return request(`/api/fake_list?${stringify(params)}`);
}

export async function removeFakeList(params) {
  const { count = 5, ...restParams } = params;
  return request(`/api/fake_list?count=${count}`, {
    method: 'POST',
    body: {
      ...restParams,
      method: 'delete',
    },
  });
}

export async function addFakeList(params) {
  const { count = 5, ...restParams } = params;
  return request(`/api/fake_list?count=${count}`, {
    method: 'POST',
    body: {
      ...restParams,
      method: 'post',
    },
  });
}

export async function updateFakeList(params) {
  // const { count = 5, ...restParams } = params;
  console.log("params is",params)

  return request(`/api/updateTraceStatus`, {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export async function fakeAccountLogin(params) {
  console.log("33333333333333333333333",params)
  return request('/api/login/account', {
    method: 'POST',
    body: params,
  });
}

export async function fakeRegister(params) {
  return request('/api/register', {
    method: 'POST',
    body: params,
  });
}

export async function queryNotices() {
  return request('/api/notices');
}

export async function getFakeCaptcha(mobile) {
  return request(`/api/captcha?mobile=${mobile}`);
}

export async function getHeroNum(params) {
  console.log("getHeroNum time is:",params)
  return request(`/api/getHeroNum?${stringify(params)}`);
}


export async function fetchTraceWarn(params) {
  console.log("getHeroNum time is:",params)
  return request(`/api/traceWarn?${stringify(params)}`);
}



export async function ticketFetch(params) {
  console.log("getHeroNum time is:",params)
  return request(`/api/getTicketStatus?${stringify(params)}`);
}


export async function updateTicket(params) {
  console.log("getHeroNum time is:",params)
  return request(`/api/updateTicketStatus?${stringify(params)}`);
}

export async function submitmaxValue(params) {
  console.log("getHeroNum time is:",params)
  return request(`/api/submitmaxValue?${stringify(params)}`);
}

export async function pAccountList(params) {
  console.log("getHeroNum time is:",params)
  return request(`/api/getpAccountList?${stringify(params)}`);
}


export async function NewPAccount(params) {
  console.log("getHeroNum time is:",params)
  return request(`/api/newPAccount?${stringify(params)}`);
}


export async function sendItem(params) {
  console.log("getHeroNum time is:",params)
  return request('/api/sendItem', {
    method: 'POST',
    body: params,
  });
}


export async function updatePwd(params) {
  console.log("UpdatePw time is:",params)
  return request(`/api/UpdatePw?${stringify(params)}`);
}


