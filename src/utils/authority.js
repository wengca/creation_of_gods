// use localStorage to store the authority info, which might be sent from server in actual project.
export function getAuthority(str) {
  // return localStorage.getItem('antd-pro-authority') || ['admin', 'user'];
  const authorityString =
    typeof str === 'undefined' ? localStorage.getItem('antd-pro-authority') : str;
  // authorityString could be admin, "admin", ["admin"]
  let authority;
  try {
    authority = JSON.parse(authorityString);
  } catch (e) {
    authority = authorityString;
  }
  if (typeof authority === 'string') {
    return [authority];
  }
  console.log("reloadAuthorized",authority)
  return authority ;
}

export function setAuthority(authority) {
  const proAuthority = typeof authority === 'string' ? [authority] : authority;
  return localStorage.setItem('antd-pro-authority', JSON.stringify(proAuthority));
}

export function setUserId(useId) {
  return localStorage.setItem('setUserId', JSON.stringify(useId));
}

export function getUserId() {
  return localStorage.getItem('setUserId');
}


export function setSession(session) {
  return localStorage.setItem('session', JSON.stringify(session));
}


export function getSession() {
  return localStorage.getItem('session');
}
